function [error, NNpa, W] = NNSoftmaxTrain(data,K,W,Nc)

% This function trains weights for NN
%fid=fopen('output.txt','W');
fprintf('WEIGHT OPTIMIZATION \n\n\n');
fprintf('Iter Train Err\t   Train Paccuracy \t\t Z \t\t\t XN \t\t\t XD\n\n');

%Initializing variables
EOld = 10000; ELeast=10000;
Nv = size(data{2,1}.train,1);
iterations=25; zOld=0; Nit = 0;
GRADCHECK = false; %% True, to check gradient calculation
% Iterations
while ( iterations ~= Nit)
Nit=Nit+1;    

[error,G,Pe,z,XN,XD] = NNSoftmaxCost(data,K,W,Nc);
if (GRADCHECK && Nit==1)
    Grad=-gradCheck(@(x) NNSoftmaxCost(data,K,x,Nc), W);
    diff = norm(G-Grad)/norm(G+Grad);
    display(diff);
end

if(error < EOld), EOld=error; WOld = W; zOld=z; 

else z=zOld*0.2;
W = WOld+(z.* G);
zOld=z;
continue
end

W = W+z*G;    
  
NNpa=(Nv-Pe)*100/Nv;
err(Nit,1)=error;
    
     
fprintf('%d    %f \t   %f \t\t %f \t\t %0.5f \t\t %0.5f \n',Nit, error,NNpa, z(1,1),XN, XD);
end
fprintf('--------------------------------------------------------------------------------------------------\n\n');
%fclose(fid);
figure(1);
plot(err);
title('error during weight optimization');
pause(0.1);

end

