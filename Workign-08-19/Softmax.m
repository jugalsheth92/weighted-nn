%% Weighted NNC with Softmax activation function
%% Created by Jugal Sheth 06/01/2016

clear; close all;

%[Tr,Ts,val,tVal,train,tTrain,Nc,Nv,NvTrain,N ] = Gongsort(2500);
 
% load('comf18.mat'); N=18; Nc=4;
% train=trainIdx(:,1:N); tTrain=trainIdx(:,end); NvTrain=size(train,1); 
% val=valIdx(:,1:N); tVal=valIdx(:,end); Nv=size(val,1); 
% test=testIdx(:,1:N); tTest=testIdx(:,end); NvTest=size(test,1); 
% Ts=[test tTest];
% Tr=[train tTrain;val tVal];

% 
trainingFile = 'Gongtrn.dat';
testingFile = 'Gongtst.tst';
% load('comf18New.mat');
N = 16;M=1;
[train,tTrain,NvTrain] = read_approx_file(trainingFile, N, M);
[test,tTest,NvTest] = read_approx_file(testingFile, N, M);

Nc = length(unique(tTrain));
N = size(train,2);


% Finding centervectors for patterns of each class
Nv_c = 10; %Number of clusters for each class
centerVectors = zeros(Nc*Nv_c, N);
centerLabels = zeros(size(centerVectors,1),1);

last = 0;
for i = 1:Nc
    mask = find(tTrain == i);
    start = last + 1; last = start + Nv_c -1;
    [centerVectors(start:last,:),~, ~] = kmeansplus(train(mask,:),Nv_c);
    centerLabels(start:last,1) = i;
end
K = Nv_c * ones(Nc,1);
% % Calculating total number of feature vectors (NVc) in each class
% K = zeros(Nc,1); % Total nos of members in each class
% for i = 1:Nc
%     K(i,1) = K(i,1)+sum(centerLabels(:,1)==i);
% end


data=cell(2,1);
data{1}.cv=centerVectors; data{1}.cvLabels=centerLabels; 
data{2}.train=train; data{2}.trainLabels=tTrain;


%Initializing weights 
sqDiff=train(:,1:N);
sqDiff=sqDiff.^2;
var=sum(sqDiff)./((size(train,1))); 
%W=1./var; 
W = randn(1,N) * sqrt(2/N);
WOld=W;

Nit=0; 
[error, NNpa, optW] = NNSoftmaxTrain_MOLF(data,K,W,Nc);
%optW = optW/max(optW);
[dataupdated, errorCenter, NNpaCenter] = centerVectorTrain(data,K,optW,Nc); 


optCVectors = [dataupdated{1,1}.cv dataupdated{1,1}.cvLabels];
CVectors = [centerVectors, centerLabels];
Ts=[test  tTest];
%Predict on testing data
%fid=fopen('output.txt','a');
fprintf(' TESTING OUTPUT\n\n\n');

[TNNpa,TPe,tError] = predict(ones(1,N),CVectors,Ts,Nc);
fprintf('Vanila NN Testing classification accuracy = %f \n Number of patterns missclassified = %d/%d\n\n', TNNpa, TPe,NvTest);

[TNNpa,TPe,tError] = predict(optW,CVectors,Ts,Nc);
fprintf('Testing classification accuracy after weight optimization= %f \n Number of patterns missclassified = %d/%d\n\n', TNNpa, TPe,NvTest);

[TNNpa,TPe,tError] = predict(optW,optCVectors,Ts,Nc);
fprintf('Testing classification accuracy after weight optimization and moving center vectors= %f \n Number of patterns missclassified = %d/%d\n\n', TNNpa, TPe,NvTest);
%fclose(fid);