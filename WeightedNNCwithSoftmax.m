%% Weighted NNC with Softmax activation function
%Created by Jugal Sheth 06/01/2016

clear;


[Tr,Ts,val,tVal,train,tTrain,Nc,Nv,NvTrain,N ] = Gongsort(2000);
% 
% fid=fopen('Weights-one','W');
% fprintf(fid,'Training Err       Training Pa          Iteration\n');
fprintf('Training Err       Training Pa            Test Err      Testing Pa\n');
data=cell(2,1);
data{1}.train=train; data{1}.trainLabels=tTrain; 
data{2}.val=val; data{2}.valLabels=tVal;

% load('comf18.mat'); N=18; Nc=4;
% train=trainIdx(:,1:N); tTrain=trainIdx(:,end); NvTrain=size(train,1); train=[train randn(NvTrain,1)];
% val=valIdx(:,1:N); tVal=valIdx(:,end); Nv=size(val,1); val=[val randn(Nv,1)];
% test=testIdx(:,1:N); tTest=testIdx(:,end); NvTest=size(test,1); test=[test randn(NvTest,1)];
% Ts=[test tTest];
% Tr=[train tTrain;valIdx tVal];
% N=N+1;
% val=val(:,1:end-1); train=train(:,1:end-1); N=N-1;

%Initializing weights 
%mean=sum(train)/(size(train,1));
%mean=sum(Tr(:,1:N))/(size(Tr,1));
%sqDiff=bsxfun(@minus,Tr(:,1:N),mean); 
sqDiff=Tr(:,1:N);
sqDiff=sqDiff.^2;
var=sum(sqDiff)./((size(Tr,1))-1); 
%var=sqrt(var);
W=1./var; 
%W=ones(1,N);
W=W.^2;
WOld=W;

%Initializing variables
EOld = 10000; z = 0.51; ELeast=10000;
iterations=100;

% Calculating total number of feature vectors (NVc) in each class
K = zeros(Nc,1); % Total nos of members in each class
for i = 1:Nc
    K(i,1) = K(i,1)+sum(tTrain(:,1)==i);
end
Nit=0; 
% Iterations
while ( iterations ~= Nit)
Nit=Nit+1;    
Pe=0; error = 0; G=zeros(1,N); GG=zeros(N,1);

repW = repmat(W,[NvTrain, 1]);

least=zeros(Nc,1);
leastIndex=zeros(Nc,1);
[cost, GRAD,PE]=NNSoftmaxCost(data,K,W,Nc);
Grad=gradCheck(@(x) NNSoftmaxCost(data,K,x,Nc), W);
diff=sum(Grad-GRAD);
for i=1:Nv
    temp = repmat(val(i,:),[NvTrain,1]);
    distance =(sum((repW.*((train - temp).^2)),2))+0.0001;
    first=0; last=0;addn=0;
    for k = 1:Nc
        first=1+last;
        last=first+K(k,1)-1;
        [least(k,1),leastIndex(k,1)] =min(distance(first:last,:));
        leastIndex(k,1)=leastIndex(k,1)+addn;
        addn=addn+K(k,1);
    end

    num = 1./(least);
    den = sum(num);
    
    Y=num./den;    
    [maxValue, class]=max(Y);
    T = zeros(Nc,1);
    T(tVal(i,1),1) = 1;     
    error = error+sum(1/Nv*((T-Y).^2)); 
    Pe = Pe+(class~=tVal(i,1));
    
   
% %     %Gradient Calculation : Need to vectorize
% %     Bn=zeros(1,N);
% %     Bd=(sum(least)).^2;
% %     C=den^2;
% %     
% %     for n = 1:N
% %         for c = 1:Nc
% %             Bn(1,n)=Bn(1,n)+((train(leastIndex(c,1),n)-val(i,n))^2);
% %         end
% %     end    
% %       
% %       for c = 1:Nc
% %           D=(-2/Nv) * (T(c)-Y(c));
% %           for n = 1:N
% %               A = den * -((train(leastIndex(c),n)-val(i,n))^2) / (least(c,1)^2);
% %               B=(Bn(1,n)*-num(c,1))/Bd;           
% %               G(1,n)=G(1,n) + D*((A-B)/C);
% %           end
% %       end
% % 
% %     if(sum(isnan(GG)))
% %         disp(i);
% %     end

%%-------------------------------------------------------------------------------

   bn=zeros(N,1);
    for j=1:Nc
     for m=1:N
        bn(m,1)=bn(m,1)-2*W(1,m)*((val(i,m)-train(leastIndex(j),m)).^2);
     end
    end
    for j=1:Nc
        for m= 1:N
            G(1,m)=G(1,m) - ((2/Nv*(T(j)-Y(j)) * ...
            ((den*- (2*W(1,m)*((val(i,m)-train(leastIndex(j),m))^2))/(least(j,1)^2)) -...
            num(j,1)*(bn(m,1)/(sum(least).^2))))/(den^2));
        end
    end

%%----------------------------------------------------------------
  
 
    
%  % Gradient Calculation
% An=zeros(Nc,N); Ad=zeros(Nc,1); Bn=zeros(N,1); Bd=0;
% Dc=zeros(Nc,1);
% 
% for i=1:Nc
%     for m=1:N
%         An(i,m)=-den*((train(leastIndex(i),m)-val(p,m))^2);
%     end;
% end
% 
% for i=1:Nc
%     for m=1:N
%         Ad(i,1)=Ad(i,1)+((W(1,m)*(train(leastIndex(i),m)-val(p,m))^2));
%     end
%     Ad(i,1)=Ad(i,1)+0.0001;
%     Ad(i,1)=Ad(i,1)^2;
% end
% 
% for i=1:Nc
%     for m=1:N
%         Bn(m)=Bn(m)-(((train(leastIndex(i),m)-val(p,m))^2));        
%     end
%     Bn(m)=Bn(m)*num(i);
% end
% bd=0;
% for i=1:Nc
%     for m=1:N
%         bd=bd+((W(1,m)*(train(leastIndex(i),m)-val(p,m))^2));
%     end
%     bd=bd+0.0001;
%     Bd=Bd+bd;
%     bd=0;
% end
% Bd=Bd^2;
% for i=1:Nc
%     for m=1:N
%         Dc(i)=Dc(i)+(W(1,m)*(train(leastIndex(i),m)-val(p,m)).^2);
%     end
%     Dc(i)=Dc(i)+0.0001;
%     Dc(i)=1/Dc(i);
% end
% Di=sum(Dc);
% g=0;
% for m=1:N
%     for i=1:Nc
%         g=-2/Nv * (T(i)-Y(i)) * (((An(i,m)/Ad(i))-(Bn(m)/Bd))/den^2);
%         G(m)=G(m)-g;
%     end
% end
end

     grad=gradientCheck(train,tTrain,val,tVal,K,W,Nc); 
     display([grad' G']);
  
    if(error<ELeast), ELeast=error; end;
    if( error == EOld && raise==false), z = z + 0.2 * z;  Nit=Nit-1;end;
    raise = false;
    if( error > ELeast +0.0*ELeast )
        W = WOld;
        z = 0.7 * z; 
        error = EOld;
        raise = true;
        Nit=Nit-1;
        continue;
    end
    WOld = W;
    W1 = W+(z.* G);
     if(sum(W1<0)), G(1,(W1<0))=0; W1 = W+(z.* G); end
     W=W1;
%    W=abs(W1);  
    EOld = error;
    NNpa=(Nv-Pe)*100/Nv;
    
    %Predict
    %[TNNpa,TPe,tError] = predict(W,Tr,Ts,Nc);
    TNNpa=0; tError=0;
    
    %fprintf(fid,'%f             %f %%            %d\n', error,NNpa, Nit);
    fprintf('%f             %f             %d\n', error,NNpa, Nit);
end

%fclose(fid);
