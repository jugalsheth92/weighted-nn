function [Tr,Ts,val,tVal,train,tTrain,Nc,Nv,NvTrain,N ] = Gongsort(tTrainNo)
%% This function sorts Gongtrn data into training and validation sets(Zero mean, unit variance)

trainingFile = 'Gongtrn.dat';
testingFile = 'Gongtst.tst';
N = 16; M = 1; Nc = 10;
[train,tTrain,NvTrain] = read_approx_file(trainingFile, N, M);
[test,tTest,NvTest] = read_approx_file(testingFile, N, M);

% Add extra random useless feature
% train = [train, randn(size(train,1),1)];
% test = [test, randn(size(train,1),1)];
% N = N+1;

% trainMean=sum(train)./NvTrain;
% train=bsxfun(@minus,train,trainMean);
% stdTrain=std(train);
% stdTrain=repmat(stdTrain,[NvTrain,1]);
% train=train./stdTrain;
% 
% test=bsxfun(@minus,test,trainMean);
% test=test./stdTrain;


Ts=[test  tTest];
Tr=[train tTrain];


V=Tr(tTrainNo+1:end,:);
V=sortrows(V,N+1); % Sorting according the classes
val = V(:,1:end-1); tVal=V(:,end);

X=Tr(1:tTrainNo,:);
X=sortrows(X,N+1); % Sorting according the classes
train = X(:,1:end-1); tTrain=X(:,end);
Nv=NvTrain-tTrainNo;
NvTrain=tTrainNo;
end

