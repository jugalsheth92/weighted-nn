function [error, z, Pe,Grads, D1, D2] = centerVectorCost(data,K,W,Nc )

% This function optimizes and moves center vector in space
% Such that intra-class center vector are close to each othr, while
% interclass center vectors are as far apart as possible

%Initializing variables
least=zeros(Nc,1);
leastIndex=zeros(Nc,1);
cv=data{1}.cv; tCv=data{1}.cvLabels; 
train=data{2}.train; tTrain=data{2}.trainLabels;
Nv=size(tTrain,1); NvTrain=size(tCv,1); N=size(cv,2);

    error=0;
    Pe=0;
    Grads = zeros(size(cv));
    D1=0; D2=0;

    result(Nv).X=zeros(Nc,N); result(Nv).Y=zeros(Nc,Nv); 
    result(Nv).T=zeros(Nc,Nv); result(Nv).num=zeros(Nc,Nv); 
    result(Nv).den=zeros(1,Nv); result(Nv).least=zeros(Nc,Nv);

    repW=repmat(W.^2,[NvTrain,1]);
    for p=1:Nv
        
        temp = repmat(train(p,:),[NvTrain,1]);
        distance =(sum((repW.*((cv - temp).^2)),2))+0.0001;
        last=0;addn=0;
        for k = 1:Nc
            first=1+last;
            last=first+K(k,1)-1;
            [least(k,1),leastIndex(k,1)] =min(distance(first:last,:));
            leastIndex(k,1)=leastIndex(k,1)+addn;
            addn=addn+K(k,1);
        end
        
        num = 1./(least);
        den = sum(num);
        
        Y=num./den;
        [~, class]=max(Y);
        T = zeros(Nc,1);
        T(tTrain(p,1),1) = 1;
        error = error+sum(1/Nv*((T-Y).^2));
        Pe = Pe+(class~=tTrain(p,1));
        X=cv(leastIndex,:);
        
        %% Gradient calculations
        
        for i = 1:size(cv,1)
            G = 0;
            if(sum((leastIndex == i)))
                
                vectorNum = find(leastIndex==i);
                dvNum = least(vectorNum);
                for u = 1:Nc
                    du = least(u);
                    if(u == vectorNum)
                        g = 2 * dvNum^-2 .* W.^2 .* (train(p,:)-X(vectorNum,:));
                        gg = g .*((den - du^-1)/den.^2) .* (T(u) - Y(u));
                    else
                        gg = -(du^-1 * 2 * dvNum^-2 * W.^2 .* (train(p,:)-X(vectorNum,:))/den^2) * (T(u) - Y(u));
                    end
                    G = G + 2*gg/(Nv);
                end
            end
            Grads(i,:) = Grads(i,:) + G; 
        end
     result(p).X=X; 
     result(p).Y=Y; 
     result(p).num=num; 
     result(p).den=den;
     result(p).least=least; 
     result(p).T=T;
     result(p).leastIndex = leastIndex;
    end
    
   for p = 1:Nv
       G = Grads(result(p).leastIndex,:);
       gradNum= sum(2 .* G .* repmat(W.^2,[Nc,1]) .*((repmat(train(p,:),[Nc,1])-result(p).X)),2);
       OD=(result(p).T)-(result(p).Y);
       OA=(result(p).den) .* gradNum./(result(p).least).^2;
       OB=(result(p).num) .* sum(gradNum./(result(p).least).^2);
       OC=(result(p).den).^2;
       dy=(OA-OB)/OC;
       d2=sum(dy.^2);
       d1=sum(dy.* OD);
       D1 = D1 + d1; 
       D2 = D2 + d2;
   end
   z = D1/(D2+1e-6);


end


