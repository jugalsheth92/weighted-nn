function [grad] = CV_Z_gradientCheck(G,data,K,W,Nc)
%Created by Jugal Sheth 06/01/2016

cv=data{1}.cv; tCv=data{1}.cvLabels; 
train=data{2}.train; tTrain=data{2}.trainLabels;
Nv=size(tTrain,1); NvTrain=size(tCv,1); N=size(cv,2);

grad=0;
EPSILON = 0.0001;

        
        theta1=EPSILON; theta2=-EPSILON;
        J1=FuncSoftmax_z(train,tTrain,cv,tCv,W,K,Nc,theta1,G);
        J2=FuncSoftmax_z(train,tTrain,cv,tCv,W,K,Nc,theta2,G);
        grad=(J1-J2)./(2*EPSILON);

end

