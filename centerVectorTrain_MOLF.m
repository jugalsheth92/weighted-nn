function [ data, error, NNpa ] = centerVectorTrain_MOLF(data,K,W,Nc,PRINTtoFILE )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

Nv = size(data{2,1}.train,1);
EOld = 10000; ELeast=10000; iterations=10; Nit = 0; posErr=0;
cvOld = data{1,1}.cv; NNpa_old = 0;
Z = zeros(size(cvOld)); zOld=Z;
fprintf('-------------------------------------------------------------------------\n')
if(PRINTtoFILE)
    fid=fopen('output.txt','a');
    fprintf(fid, 'MOVING CENTER VECTORS \n\n\n');
    fprintf(fid, 'Iter Train Err\t   Val Paccuracy \n\n');
else
    fprintf('MOVING CENTER VECTORS \n\n\n');
    fprintf('Iter Train Err\t   Val Paccuracy \n\n');
end

while (iterations ~= Nit)
    Nit=Nit+1;    
    [error, Z,Pe, Grads] = centerVectorCost_MOLF(data,K,W,Nc);
    Z = Z';
    Z = repmat(Z,[1 size(Grads,2)]);
%     for i = 1:Nc
%         Z(cv_labels==i,:) = z(i);
%     end
    if(error < EOld) 
        EOld=error; cvOld = data{1,1}.cv; zOld=Z; 
    else
        Z=zOld*0.1;
        data{1,1}.cv = cvOld + Z.*Grads;
        zOld=Z; 
        %iterations = iterations + 1;
        continue 
    end
    data{1,1}.cv = data{1,1}.cv + Z .*Grads;
  
    NNpa=(Nv-Pe)*100/Nv;
    posErr=posErr+1;
    err(posErr,1)=error;
    
    if(NNpa_old <= NNpa), NNpa_old = NNpa; cvBest = data{1,1}.cv; end;

    if(PRINTtoFILE)
        fprintf( fid,'%d    %f \t   %f \t\t  \n',Nit, error,NNpa);
    else
        fprintf('%d    %f \t   %f \t\t  \n',Nit, error,NNpa);    
    end
   
end
figure(2);
title('Training error during center vector optimization');    
plot(err);
fprintf('--------------------------------------------------------------------------------------------------\n\n');
if(PRINTtoFILE), fclose(fid); end;
data{1,1}.cv = cvBest;
end

