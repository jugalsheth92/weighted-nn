function [error, NNpa, W] = WO_Train_MOLF(data,K,W,Nc,PRINTtoFILE,Ts)

% This function trains weights for NN
if(PRINTtoFILE)
    fid=fopen('output.txt','W');
    fprintf(fid, 'WEIGHT OPTIMIZATION \n\n\n');
    fprintf(fid, 'Iter Train Err\t   Train Paccuracy \n\n');
else
    fprintf('WEIGHT OPTIMIZATION \n\n\n');
    fprintf('Iter Train Err\t   Train Paccuracy \n\n');
end

%Initializing variables
EOld = 10000; ELeast=10000;
Nv = size(data{2,1}.train,1);
iterations=30; eOld=0; Nit = 0; posErr=0; NNpaOld = 0;
GRADCHECK = false; %% True, to check gradient calculation

% Iterations
while ( iterations ~= Nit)
Nit=Nit+1;    
% Calculating gradients, errors, and update vector (e)
[error,e,Pe] = WO_Cost_MOLF(data,K,W,Nc);
if (GRADCHECK && Nit==1)
    Grad=-gradCheck(@(x) WO_Cost_MOLF(data,K,x,Nc), W);
    diff = norm(G-Grad)/norm(G+Grad);
    display(diff);
end


NNpa=(Nv-Pe)*100/Nv;
if(NNpa > NNpaOld)
    NNpaOld = NNpa; 
    W_best = W;
end
if(error <= EOld), EOld=error; WOld = W; eOld=e; 

else e=eOld*0.1;
W = WOld+(e);
eOld=e; Nit = Nit-1;
continue
end
posErr=posErr+1;
W = W+e;    
   

err(posErr,1)=error;
    
if(PRINTtoFILE)     
    fprintf( fid,'%d    %f \t   %f \t\t\n',Nit, error,NNpa);
else
    fprintf('%d    %f \t   %f \t\t\n',Nit, error,NNpa);
end
end
W = W_best;
fprintf('--------------------------------------------------------------------------------------------------\n\n');
if(PRINTtoFILE), fclose(fid); end;
figure(1);
plot(err);
title('Training error during weight optimization');
pause(0.1);

end

