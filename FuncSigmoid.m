function [ err ] = FuncSigmoid( train,tTrain,test,tTest,W,K,Nc )


numgrad=zeros(size(W));
NvTrain=size(tTrain,1);
NvTest=size(tTest,1);
repW = repmat(W,[NvTrain, 1]);

err=0;
least=zeros(Nc,1);
leastIndex=least;
for i=1:NvTest
    temp = repmat(test(i,:),[NvTrain,1]);
    distance =(sum((repW.*((train - temp).^2)),2));
    last=0;
    for j = 1:Nc
        first=1+last;
        last=first+K(j,1)-1;
        [least(j,1),leastIndex(j,1)] =min(distance(first:last,:));
        leastIndex(j,1)=leastIndex(j,1)+(j-1)*K(j,1);
    end
    
    Y=2./(1+exp(least)); 
    T=zeros(Nc,1);
    T(tTest(i,1),1) = 1;    
    err = err+sum(1/NvTest*((T-Y).^2)); 
end
end
