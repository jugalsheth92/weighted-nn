function [error, NNpa, W] = WO_Train_OLF(data,K,W,Nc,PRINTtoFILE,Ts)

% This function trains weights for NN
if(PRINTtoFILE)
    fid=fopen('output.txt','W');
    fprintf(fid, 'WEIGHT OPTIMIZATION \n\n\n');
    fprintf(fid, 'Iter Train Err\t   Train Paccuracy \n\n');
else
    fprintf('WEIGHT OPTIMIZATION \n\n\n');
    fprintf('Iter Train Err\t   Train Paccuracy \n\n');
end
%Initializing variables
EOld = 10000; ELeast=10000;
Nv = size(data{2,1}.train,1);
iterations=25; zOld=0; Nit = 0;
GRADCHECK = true; %% True, to check gradient calculation
% Iterations
while ( iterations ~= Nit)
Nit=Nit+1;    

[error,G,Pe,z,XN,XD] = WO_Cost_OLF(data,K,W,Nc);
if (GRADCHECK && Nit==1)
    Grad=-gradCheck(@(x) WO_Cost_OLF(data,K,x,Nc), W);
    diff = sumsqr(G-Grad)/(norm(G)*norm(Grad));
    display(diff);
end

if(error < EOld), EOld=error; WOld = W; zOld=z; 

else z=zOld*0.2;
W = WOld+(z.* G);
zOld=z;
continue
end

W = W+z*G;    
  
NNpa=(Nv-Pe)*100/Nv;
err(Nit,1)=error;
    
     
if(PRINTtoFILE)     
    fprintf( fid,'%d    %f \t   %f \t\t\n',Nit, error,NNpa);
else
    fprintf('%d    %f \t   %f \t\t\n',Nit, error,NNpa);
end
end
fprintf( '--------------------------------------------------------------------------------------------------\n\n');
figure(1);
plot(err);
title('error during weight optimization');
pause(0.1);

end

