function [ error ] = FuncSoftmax_z(train,tTrain,cv,tCv,W,K,Nc,z,G )
%Created by Jugal Sheth 06/01/2016


Nv =size(tTrain,1);
NvTrain=size(tCv,1);
repW=repmat(W.^2,[NvTrain,1]);
error=0;
least=zeros(Nc,1);
leastIndex=least;
    for p=1:Nv        
        temp = repmat(train(p,:),[NvTrain,1]);
        outPut = (temp-(cv+z*G));
        distance =(sum((repW.*(outPut.^2)),2))+0.0001;
        last=0;addn=0;
        for k = 1:Nc
            first=1+last;
            last=first+K(k,1)-1;
            [least(k,1),leastIndex(k,1)] =min(distance(first:last,:));
            leastIndex(k,1)=leastIndex(k,1)+addn;
            addn=addn+K(k,1);
        end
        
        num = 1./(least);
        den = sum(num);
        
        Y=num./den;
        T = zeros(Nc,1);
        T(tTrain(p,1),1) = 1;
        error = error+sum(1/Nv*((T-Y).^2));
    end
end
