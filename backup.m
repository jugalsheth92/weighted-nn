%% Weighted NNC with Sigmoid activation function

trainingFile = 'Gongtrn.dat';
testingFile = 'Gongtst.tst';
N = 16; M = 1; Nc = 10; jFold=1;
[train,tTrain,NvTrain] = read_approx_file(trainingFile, N, M);
[test,tTest,NvTest] = read_approx_file(testingFile, N, M);
X=[test, tTest];
X = sortrows(X,N+1); % Sorting according the classes
test = X(:,1:end-1); tTest=X(:,end);



%Initializing weights 
mean=sum(train)/size(train,1);
sqDiff=bsxfun(@minus,train,mean); 
sqDiff=sqDiff.^2;
var=sum(sqDiff)/size(train,1); 
W=1./var; WOld=W;

% Splitting training and validation set
X = [train, tTrain];
NvVal = size(X,1)/jFold;
Nv=NvTrain-NvVal;
for fold = 1:jFold
    train=[]; val=[];
    start = ((fold-1)*NvVal)+1; en = start+(NvVal-1);
    if(start ~= 1), train=X(1:start-1,:); end; 
    val = X(start:en,:);
    train = [train;X(en+1:end,:)];  

 XVal = sortrows(val,N+1); % Sorting training set according the classes
 val = XVal(:,1:end-1); tVal=XVal(:,end);

 Xtrain = sortrows(train,N+1); % Sorting validation set according the classes
 train = Xtrain(:,1:end-1); tTrain=Xtrain(:,end);

%Initializing variables
PeOld = 10000; z = 0.01; error=0; PeLeast=10000;
iterations=20; GradEnergy=zeros(iterations,1); prod=0; Gp=zeros(N,1);
 
% Calculating total number of feature vectors (NVc) in each class
K = zeros(Nc,1); % Total nos of members in each class
for i = 1:Nc
    K(i,1) = K(i,1)+sum(Xtrain(:,N+1)==i);
end
for Nit =1:iterations
Pe=0; G=zeros(1,N);
repW = repmat(W,[Nv, 1]);

least=zeros(Nc,1);
leastIndex=least;
for i=1:NvVal
    temp = repmat(val(i,:),[Nv,1]);
    distance =(sum((repW.*(train - temp).^2),2));
    first=0; last=0;
    for j = 1:Nc
        first=1+last;
        last=first+K(j,1)-1;
        [least(j,1),leastIndex(j,1)] =min(distance(first:last,:));
        leastIndex(j,1)=leastIndex(j,1)+(j-1)*K(j,1);
    end
    
    fnet=2./(1+exp(least));
    [maxValue, class]=max(fnet);
    T=zeros(Nc,1);
    T(tVal(i,1),1) = 1;    
    error = error+sum(1/Nv*((T-fnet).^2)); 
    Pe=Pe+(class~=tVal(i,1));
    
    %Gradient (Remove Forloops).
    denominator = exp(least)./((1+exp(least)).^2);
    denominator(isnan(denominator),1)=0;
   
        
    for c = 1:Nc;
     for n = 1:N     
       G(1,n) = G(1,n) - (2/Nc) * (T(c) - fnet(c,1)) *(train(leastIndex(c,1),n)-val(i,n)).^ 2 * denominator(c,1);
     end;

   end
end
    GradEnergy(Nit,1)=sum((G.^2));
    prod=prod+G*Gp;
    Gp=G';
    if(Pe<PeLeast), PeLeast=Pe; end;
    if( Pe == PeOld), z = z + 0.1 * z; end;
        
    if( Pe > PeLeast )
        W = WOld;
        disp(Pe);
        z = 0.5 * z; 
        Pe = PeOld;
        disp('Pe increased');
        continue;
    end
    WOld = W;
    W(1,:) = W(1,:)+(z.* G);
    
    PeOld = Pe;
    text = sprintf('# of missclassification : %d', Pe);
    disp(text);
    disp('-------------------------')
 
 end
end
