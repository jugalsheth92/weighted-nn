function [ data, error, NNpa ] = centerVectorTrain(data,K,W,Nc )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

Nv = size(data{2,1}.train,1);
EOld = 10000; ELeast=10000; iterations=15; zOld=0; Nit = 0; posErr=0;
cvOld = data{1,1}.cv;
fprintf('-------------------------------------------------------------------------\n')
%fid=fopen('output.txt','a');
fprintf('MOVING CENTER VECTORS \n\n\n');
fprintf('Iter Train Err\t   Train Paccuracy \t\t Z \t\t\t XN \t\t\t XD\n\n');
check = 5;
while (iterations ~= Nit)
    Nit=Nit+1;    
    [error, z,Pe, Grads, XN, XD] = centerVectorCost(data,K,W,Nc); 
    
    if(error < EOld), EOld=error; cvOld = data{1,1}.cv; zOld=z; 
    else z=zOld*0.2;
    %check = check - 1;
    data{1,1}.cv = cvOld + z* Grads;
    zOld=z; 
    continue 
    end
    data{1,1}.cv = data{1,1}.cv + z* Grads;
  
    NNpa=(Nv-Pe)*100/Nv;
    posErr=posErr+1;
    err(posErr,1)=error;
    
    fprintf('%d    %f \t   %f \t\t %f \t\t %0.5f \t\t %0.5f \n',Nit, error,NNpa, z,XN, XD);
    title('error during center vector optimization');
   
end
figure(2);
plot(err);
fprintf('--------------------------------------------------------------------------------------------------\n\n');
%fclose(fid);
end

