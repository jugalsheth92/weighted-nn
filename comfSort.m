function [ Tr,Ts,val,tVal,train,tTrain,Nc,Nv,NvTrain,N ] = comfSort()

trainingFile = 'Comf18.tra';
N = 18; M = 1; Nc = 4;
[X,T,Nvt] = read_approx_file(trainingFile, N, M);
NvTest=4130; NvTrain=4131; Nv=4131;
X=[X T]; 
X=sortrows(X,N+1);
X=X';
K = zeros(Nc,1); % Total nos of members in each class
for i = 1:Nc
    K(i,1) = K(i,1)+sum(T(:,1)==i);
end
trainIdx=[]; valIdx=[]; testIdx=[];
last=0;
for i=1:Nc
    start=last+1;
    last = start + K(i,1)-1;
  [t,v,tst] = divideint(X(:,start:last),0.33,0.33,0.33);
  trainIdx=[trainIdx;t']; valIdx=[valIdx;v']; testIdx=[testIdx;tst'];
end

