function [error, z, Pe,Grads] = centerVectorCost_MOLF(data,K,W,Nc )

% This function optimizes and moves center vector in space
% Such that intra-class center vector are close to each othr, while
% interclass center vectors are as far apart as possible

%Initializing variables
least=zeros(Nc,1);
leastIndex=zeros(Nc,1);
cv=data{1}.cv; tCv=data{1}.cvLabels; 
train=data{2}.train; tTrain=data{2}.trainLabels;
Nv=size(tTrain,1); NvTrain=size(tCv,1); N=size(cv,2);

    error=0;
    Pe=0;
    Grads = zeros(size(cv));
    D1=zeros(NvTrain,1); D2=zeros(NvTrain,NvTrain);

    result(Nv).X=zeros(Nc,N); result(Nv).Y=zeros(Nc,Nv); 
    result(Nv).T=zeros(Nc,Nv); result(Nv).num=zeros(Nc,Nv); 
    result(Nv).den=zeros(1,Nv); result(Nv).least=zeros(Nc,Nv);

    repW=repmat(W.^2,[NvTrain,1]);
    for p=1:Nv
        
        temp = repmat(train(p,:),[NvTrain,1]);
        distance =(sum((repW.*((temp-cv).^2)),2))+0.0001;
        last=0;addn=0;
        for k = 1:Nc
            first=1+last;
            last=first+K(k,1)-1;
            [least(k,1),leastIndex(k,1)] =min(distance(first:last,:));
            leastIndex(k,1)=leastIndex(k,1)+addn;
            addn=addn+K(k,1);
        end
        
        num = 1./(least);
        den = sum(num);
        
        Y=num./den;
        [~, class]=max(Y);
        T = zeros(Nc,1);
        T(tTrain(p,1),1) = 1;
        error = error+sum(1/Nv*((T-Y).^2));
        Pe = Pe+(class~=tTrain(p,1));
        X=cv(leastIndex,:);
        
        %% Gradient calculations
        
%         for i = 1:size(cv,1)
%             G = 0;
%             if(sum((leastIndex == i)))
%                 
%                 vectorNum = find(leastIndex==i);
%                 dvNum = least(vectorNum);
%                 for u = 1:Nc
%                     du = least(u);
%                     if(u == vectorNum)
%                         g = 2 * dvNum^-2 .* W.^2 .* (train(p,:)-X(vectorNum,:));
%                         gg = g .*((den - du^-1)/den.^2);
%                     else
%                         gg = -(du^-1 * 2 * dvNum^-2 * W.^2 .* (train(p,:)-X(vectorNum,:))/den^2);
%                     end
%                     G = G + 2/Nv * gg .* (T(u) - Y(u));
%                 end
%             end
%             Grads(i,:) = Grads(i,:) + G; 
%         end
for i = 1:Nc
    for u = 1:Nc
        if(u == i)
            g = 2 * least(i)^-2 .*W.^2 .* (train(p,:)-X(u,:));
            gg = g .*((den - num(i))/den.^2);
        else
            gg = -(num(i) * 2 * least(u)^-2 .* W.^2 .* (train(p,:)-X(u,:))/den.^2);
        end
        Grads(leastIndex(u),:) = Grads(leastIndex(u),:) + (2/Nv .*(T(i) - Y(i)) .* gg);
    end
end
     result(p).X=X; 
     result(p).Y=Y; 
     result(p).num=num; 
     result(p).den=den;
     result(p).least=least; 
     result(p).T=T;
     result(p).leastIndex = leastIndex;
    end
    
   for p = 1:Nv
       OG = Grads(result(p).leastIndex,:);
       OD= (result(p).T)-(result(p).Y);
       %OC=(result(p).den).^2;
       for i = 1:Nc    
           dy = zeros(NvTrain,1);
           for u = 1:Nc               
               if(u == i)
                   g = 2 * result(p).least(i).^-2 * sum(OG(i,:) .* W.^2 .* (train(p,:)-result(p).X(i,:))); 
                   gg = g .* (result(p).den - result(p).num(i))/(result(p).den.^2);
               else
                   gg = -(result(p).num(i) * 2 * result(p).least(u)^-2 .* sum(W.^2 .* OG(u,:) ...
                       .* (train(p,:) - result(p).X(u,:))))/(result(p).den.^2);
               end
               dy(result(p).leastIndex(u))=gg;
               D1(result(p).leastIndex(u)) = D1(result(p).leastIndex(u)) + (OD(i) * dy(result(p).leastIndex(u)));
           end
           D2 = D2 + (dy * dy');
       end

   end
   D1 = D1 * 2/Nv;
   D2 = D2 * 2/Nv;
   z = (OLS2(D2,D1));
   %z_check = CV_ZIK_gradientCheck(Grads,data,K,W,Nc);


end


