%% Weighted NNC with Sigmoid activation function

[Tr,Ts,val,tVal,train,tTrain,Nc,Nv,NvTrain,N ] = Gongsort(2000);

fid=fopen('comf18 W^2.txt','W');
fprintf(fid,'Training Err       Training Pa            Test Err      Testing Pa\n');
fprintf('Training Err       Training Pa            Test Err      Testing Pa\n');

% load('conf18.mat'); N=18; Nc=4;
% train=trainIdx(:,1:N); tTrain=trainIdx(:,end); NvTrain=size(train,1);
% val=valIdx(:,1:N); tVal=valIdx(:,end); Nv=size(val,1);
% test=testIdx(:,N); tTest=testIdx(:,end); NvTest=size(test,1);
% Ts=testIdx;
% Tr=[trainIdx;valIdx];

%Initializing weights 
sqDiff=Tr(:,1:N);
sqDiff=sqDiff.^2;
var=sum(sqDiff)./((size(Tr,1))-1); 
W=1./var; 
WOld=W;

%Initializing variables
EOld = 10000; z = 30; ELeast=10000;
iterations=500;

% Calculating total number of feature vectors (NVc) in each class
K = zeros(Nc,1); % Total nos of members in each class
for i = 1:Nc
    K(i,1) = K(i,1)+sum(tTrain(:,1)==i);
end
Nit=0; raise=false;  
while ( iterations ~= Nit)
Nit=Nit+1;    
Pe=0; G=zeros(1,N); error = 0; g=zeros(1,N);
repW = repmat(W,[NvTrain, 1]);
%repW = repW.^2;
least=zeros(Nc,1);
leastIndex=least;
for i=1:Nv
    temp = repmat(val(i,:),[NvTrain,1]);
    distance =(sum((repW.*((train - temp).^2)),2))+0.0001;
    first=0; last=0;
    for k = 1:Nc
        first=1+last;
        last=first+K(k,1)-1;
        [least(k,1),leastIndex(k,1)] =min(distance(first:last,:));
        leastIndex(k,1)=leastIndex(k,1)+(k-1)*K(k,1);
    end
    
    Y=2./(1+exp(least));
    [maxValue, class]=max(Y);
    T = zeros(Nc,1);
    T(tVal(i,1),1) = 1;     
    error = error+sum(1/Nv*((T-Y).^2)); 
    Pe = Pe+(class~=tVal(i,1));
    
    %Gradient (Remove Forloops).
    denominator = - 2*exp(least)./((1+exp(least)).^2);
    denominator(isnan(denominator),1)=0;
    gw=repmat(W,[Nc,1]);
    A=zeros(1,Nc);
    for c=1:Nc
        A(1,c)=2*(sum(W.*(train(leastIndex(c,1),:)-val(i,:)).^ 2,2));
    end
        
    for c = 1:Nc
     for n = 1:N     
        G(1,n) = G(1,n) - (2/Nv) * (T(c) - Y(c,1))* (train(leastIndex(c,1),n)-val(i,n)).^ 2 * denominator(c,1);
         %G(1,n) = G(1,n) - (2/Nv) * (T(c) - Y(c,1))* 2*W(1,n)*(train(leastIndex(c,1),n)-val(i,n)).^ 2 * denominator(c,1);
     end;
   end
end
    
    if(error<ELeast), ELeast=error; end;
    if( error == EOld && raise==false), z = z + 0.5 * z;  Nit=Nit-1;end;
    raise = false;
    if( error > ELeast +0.0*ELeast )
        W = WOld;
        z = 0.3 * z; 
        error = EOld;
        raise = true;
        Nit=Nit-1;
        continue;
    end
    WOld = W;
    W = W-(z.* G);
    if(sum(W==0)), G(1,W==0)=0; W = W-(z.* G); end
       
    EOld = error;
    %text = sprintf('Training Error: %0.10f', error);
    NNpa=(Nv-Pe)*100/Nv;
    %[TNNpa,TPe,tError] = predict(W,Tr,Ts,Nc);
    TNNpa=0; tError=0;
    fprintf('%f             %f             %f         %f \n', error,NNpa,tError,TNNpa)

end