%% Weighted NNC with Softmax activation function
%Created by Jugal Sheth 06/01/2016

clear;


[Tr,Ts,val,tVal,train,tTrain,Nc,Nv,NvTrain,N ] = Gongsort(2000);
% 
% fid=fopen('comf18 W^2.txt','W');
% fprintf(fid,'Training Err       Training Pa            Test Err      Testing Pa\n');
fprintf('Training Err       Training Pa            Test Err      Testing Pa\n');


% load('conf18.mat'); N=18; Nc=4;
% train=trainIdx(:,1:N); tTrain=trainIdx(:,end); NvTrain=size(train,1); train=[train randn(NvTrain,1)];
% val=valIdx(:,1:N); tVal=valIdx(:,end); Nv=size(val,1); val=[val randn(Nv,1)];
% test=testIdx(:,1:N); tTest=testIdx(:,end); NvTest=size(test,1); 
% test=[test randn(NvTest,1)];
% Ts=[test tTest];
% Tr=[train tTrain;valIdx tVal];
% N=N+1;

%Initializing weights 
%mean=sum(train)/(size(train,1));
mean=sum(Tr(:,1:N))/(size(Tr,1));
sqDiff=bsxfun(@minus,Tr(:,1:N),mean); 
sqDiff=sqDiff.^2;
var=sum(sqDiff)/(size(Tr,1)); 
W=1./var; 
%W=ones(1,N);
WOld=W;


% nosEx=size(Tr,1);
% % Splitting data into training and validation
% jStart=1;
% for l=1:jFold-1
%     X=[];
%     jStart=(l-1)*nosEx/jFold + 1;
%     jEnd=jStart+nosEx/jFold-1;
%     V=Tr(jStart:jEnd,:);
%     if(jStart~=1), X=Tr(1:jStart-1,:); end;
%     if(jEnd==nosEx), X=Tr(1:jStart-1,:);
%     else X=[X; Tr(jEnd+1:end,:)];end
% 
%     
% V=sortrows(V,N+1); % Sorting according the classes
% val = V(:,1:end-1); tVal=V(:,end);
% X=sortrows(X,N+1); % Sorting according the classes
% train = X(:,1:end-1); tTrain=X(:,end);
% 
% NvTrain=size(train,1);
% Nv=size(val,1);

%Initializing variables
EOld = 10000; z = 0.05; ELeast=10000;
iterations=25;

% Calculating total number of feature vectors (NVc) in each class
K = zeros(Nc,1); % Total nos of members in each class
for i = 1:Nc
    K(i,1) = K(i,1)+sum(tTrain(:,1)==i);
end
Nit=0; raise=false;  
% Iterations
while ( iterations ~= Nit)
Nit=Nit+1;    
Pe=0; error = 0; G=zeros(1,N);
repW = repmat(W,[NvTrain, 1]);
%repW=repW.^2;
least=zeros(Nc,1);
leastIndex=least;

for i=1:Nv
    temp = repmat(val(i,:),[NvTrain,1]);
    distance =(sum((repW.*((train - temp).^2)),2))+0.0001;
    first=0; last=0;addn=0;
    for k = 1:Nc
        first=1+last;
        last=first+K(k,1)-1;
        [least(k,1),leastIndex(k,1)] =min(distance(first:last,:));
        leastIndex(k,1)=leastIndex(k,1)+addn;
        addn=addn+K(k,1);
    end

    num = 1./(least);
    den = sum(num);
    
    Y = num./den;
    
    [maxValue, class]=max(Y);
    T = zeros(Nc,1);
    T(tVal(i,1),1) = 1;     
    error = error+sum(1/Nv*((T-Y).^2)); 
    Pe = Pe+(class~=tVal(i,1));
    
   
   %Gradient Calculation : Need to vectorize
    Bn=zeros(1,N);
    Bd=(sum(least)).^2;
    C=den^2;
    
    for n = 1:N
        for c = 1:Nc
            Bn(1,n)=Bn(1,n)-((train(leastIndex(c,1),n)-val(i,n))^2);
        end
    end;
    
    
      
      for c = 1:Nc
          D=(-2/Nv) * (T(c)-Y(c));
          for n = 1:N
              A = den * -((train(leastIndex(c),n)-val(i,n))^2) / (least(c,1)^2);
              B=(Bn(1,n)*num(c,1))/Bd;           
              G(1,n)=G(1,n) + D*((A-B)/C);
          end
      end

    if(sum(isnan(G)))
        disp(i);
    end


%     %Gradient Calculation : Need to vectorize
%     bn=zeros(N,1);
%     for j=1:Nc
%      for m=1:N
%         bn(m,1)=bn(m,1)-2*W(1,m)*((val(i,m)-train(leastIndex(j),m)).^2);
%      end
%     end
%     for j=1:Nc
%         for m= 1:N
%             G(1,m)=G(1,m) - ((2/Nv*(T(j)-Y(j)) *...
%             ((den*- (2*W(1,m)*((val(i,m)-train(leastIndex(j),m))^2))/(least(j,1)^2)) -...
%             num(j,1)*(bn(m,1)/(sum(least).^2))))/(den^2));
%         end
%     end
% 
% 

% % Gradient Calculation
% An=zeros(Nc,N); Ad=zeros(Nc,1); Bn=zeros(Nc,1); Bd=0;
% Dc=zeros(Nc,1);
% for c=1:Nc
%     for n=1:N
%         An(c,n)=-(train(leastIndex(c),n)-val(i,n)).^2;
%     end;
% end
% 
% for c=1:Nc
%     for n=1:N
%         Ad(c,1)=Ad(c,1)+(W(1,n)*(train(leastIndex(c),n)-val(i,n)).^2);
%     end
% end
% 
% for c=1:Nc
%     for n=1:N
%         Bn(c,1)=Bn(c,1)-((train(leastIndex(c),n)-val(i,n)).^2)        ;
%     end
% end;
% 
% for c=1:Nc
%     for n=1:N
%         Bd=Bd+(W(1,n)*(train(leastIndex(c),n)-val(i,n)).^2);
%     end
% end
% 
% for c=1:Nc
%     for n=1:N
%         Dc(c,1)=Dc(c,1)+(W(1,n)*(train(leastIndex(c),n)-val(i,n)).^2);
%     end
%     Dc(c,1)=1/Dc(c,1);
% end
% Di=sum(Dc);
% 
% for n=1:N
%     for c=1:Nc
%         g=-2/Nv * (T(c)-Y(c)) * (((Di*An(c,n)/Ad(c,1))-(Bn(c,1)*Dc(c,1)/Bd))/Di^2);
%         G(1,n)=G(1,n)-g;
%     end
% end
%     
%         




end

     grad=gradCheck(train,tTrain,val,tVal,K,W,Nc); 
     display([grad' G']);
%   
    if(error<ELeast), ELeast=error; end;
    if( error == EOld && raise==false), z = z + 0.5 * z;  Nit=Nit-1;end;
    raise = false;
    if( error > ELeast +0.0*ELeast )
        W = WOld;
        z = 0.6 * z; 
        error = EOld;
        raise = true;
        Nit=Nit-1;
        continue;
    end
    WOld = W;
    W1 = W-(z.* G);
     if(sum(W1<0)), G(1,(W1<0))=0; W1 = W-(z.* G); end
     W=W1;
%    W=abs(W1);  
    EOld = error;
    NNpa=(Nv-Pe)*100/Nv;
    
    %Predict
    [TNNpa,TPe,tError] = predict(W,Tr,Ts,Nc);
    
    %fprintf(fid,'%f             %f %%             %f         %f %% \n', error,NNpa,tError,TNNpa);
    fprintf('%f             %f             %f         %f \n', error,NNpa,tError,TNNpa);
end
%fclose(fid);
