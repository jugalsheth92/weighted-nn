function [ NNpa,Pe, error ] = predict(W, T, Ts, Nc)

N=size(T,2)-1;
T=sortrows(T,N+1);
train=T(:,1:end-1); tTrain=T(:,end);
test=Ts(:,1:end-1); tTest=Ts(:,end);
NvTrain=size(train,1);
NvTest=size(test,1);
repW = repmat(W.^2,[NvTrain, 1]);

Nv=NvTrain;
% Calculating total number of feature vectors (NVc) in each class
K = zeros(Nc,1); % Total nos of members in each class
for i = 1:Nc
    K(i,1) = K(i,1)+sum(tTrain(:,1)==i);
end
Pe=0; error=0;
for i=1:NvTest
    temp = repmat(test(i,:),[NvTrain,1]);
    distance =(sum((repW.*((train - temp).^2)),2))+0.0001;
    last=0;addn=0;
    for k = 1:Nc
        first=1+last;
        last=first+K(k,1)-1;
        [least(k,1),leastIndex(k,1)] =min(distance(first:last,:));
        leastIndex(k,1)=leastIndex(k,1)+addn;
        addn=addn+K(k,1);
    end

    num = 1./(least);
    den = sum(num);
    
    Y=num./den;    
    [~, class]=max(Y);
    T = zeros(Nc,1);
    T(tTest(i,1),1) = 1;     
    error = error+sum(1/Nv*((T-Y).^2)); 
    Pe = Pe+(class~=tTest(i,1));
end
NNpa = (NvTest-Pe)*100/NvTest;  %nearest neighbor percentage accuracy
%fprintf('-- The percentage accuracy(naive) is %f \n', NNpa);
%fprintf('-- # of missclassification is %d \n', Pe');

end

