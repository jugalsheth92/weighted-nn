function [ error, e, Pe] = NNSoftmaxCost_MOLF( data,K,W,Nc)
%% This function computes the cost and gradients
%% Created by Jugal Sheth 06/01/2016

least=zeros(Nc,1);
leastIndex=zeros(Nc,1);
cv=data{1}.cv; tCV=data{1}.cvLabels; 
train=data{2}.train; tTrain=data{2}.trainLabels;
Nv=size(tTrain,1); NvTrain=size(tCV,1); N=size(cv,2);
G=zeros(1,N); H = zeros(N,N);
error=0;
Pe=0;
result(Nv).X=zeros(Nc,N); result(Nv).Y=zeros(Nc,Nv); 
result(Nv).T=zeros(Nc,Nv); result(Nv).num=zeros(Nc,Nv); 
result(Nv).den=zeros(1,Nv); result(Nv).least=zeros(Nc,Nv);

repW=repmat(W.^2,[NvTrain,1]);
for p=1:Nv
    
    temp = repmat(train(p,:),[NvTrain,1]);
    distance =(sum((repW.*((cv - temp).^2)),2))+0.0001;
    last=0;addn=0;
    for k = 1:Nc
        first=1+last;
        last=first+K(k,1)-1;
        [least(k,1),leastIndex(k,1)] =min(distance(first:last,:));
        leastIndex(k,1)=leastIndex(k,1)+addn;
        addn=addn+K(k,1);
    end

    num = 1./(least);
    den = sum(num);
    
    Y=num./den;    
    [~, class]=max(Y);
    T = zeros(Nc,1);
    T(tTrain(p,1),1) = 1;     
    error = error+sum(1/Nv*((T-Y).^2)); 
    Pe = Pe+(class~=tTrain(p,1));
    X=cv(leastIndex,:); 
   
    %% Gradient Calculation
    
    Bn=zeros(N,1);
    DWY = zeros(Nc,N);
    for m=1:N
     for j=1:Nc
        bn=(2*W(1,m)*((train(p,m)-X(j,m)).^2))/(least(j).^2);
        Bn(m,1)=Bn(m,1)+bn;
     end
    end
    
    for j=1:Nc
       for m=1:N        
            D=(T(j)-Y(j));
            A=den*- (2*W(1,m)*((train(p,m)-X(j,m))^2))/(least(j,1)^2);
            B=-num(j)*Bn(m);
            C=den.^2;
            DWY(j,m) = (A-B)/C;
            G(1,m)=G(1,m) + (D*DWY(j,m));
        end
        for m = 1:N    
            for n = 1:N
                H(n,m) = H(n,m) +  (DWY(j,m) * DWY(j,n));
            end
        end        
    end
    result(p).X=X; 
    result(p).Y=Y; 
    result(p).num=num; 
    result(p).den=den;
    result(p).least=least; 
    result(p).T=T;
end

G = G * 2/Nv;
H = H * 2/Nv;
e=(OLS2(H,G'));
end


