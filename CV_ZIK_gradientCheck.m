function [grad] = CV_ZIK_gradientCheck(G,data,K,W,Nc)
%Created by Jugal Sheth 06/01/2016

cv=data{1}.cv; tCv=data{1}.cvLabels; 
train=data{2}.train; tTrain=data{2}.trainLabels;
Nv=size(tTrain,1); NvTrain=size(tCv,1); N=size(cv,2);
start = 1;
grad=zeros(1,Nc);
EPSILON = 0.0001;
for i = 1:NvTrain
    theta1 = zeros(NvTrain,N); theta2 = zeros(NvTrain,N);
    theta1(i,:) = EPSILON; theta2(i,:) = -EPSILON;
    J1=FuncSoftmax_zi(train,tTrain,cv,tCv,W,K,Nc,theta1,G);
    J2=FuncSoftmax_zi(train,tTrain,cv,tCv,W,K,Nc,theta2,G);
    grad(1,i)=(J1-J2)./(2*EPSILON);

end

