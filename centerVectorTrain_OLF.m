function [ data, error, NNpa ] = centerVectorTrain_OLF(data,K,W,Nc,PRINTtoFILE )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

Nv = size(data{2,1}.train,1);
EOld = 10000; ELeast=10000; iterations=30; zOld=0; Nit = 0; posErr=0;
cvOld = data{1,1}.cv; NNpa_old = 0;
fprintf('-------------------------------------------------------------------------\n')
if(PRINTtoFILE)
    fid=fopen('output.txt','a');
    fprintf(fid, 'MOVING CENTER VECTORS \n\n\n');
    fprintf(fid, 'Iter Train Err\t   Val Paccuracy \t\t Z \t\t\t XN \t\t\t XD\n\n');
else
    fprintf('MOVING CENTER VECTORS \n\n\n');
    fprintf('Iter Train Err\t   Val Paccuracy \t\t Z \t\t\t XN \t\t\t XD\n\n');
end

while (iterations ~= Nit)
    Nit=Nit+1;    
    [error, z,Pe, Grads, XN, XD] = centerVectorCost_OLF(data,K,W,Nc); 

    if(error <= EOld), EOld=error; cvOld = data{1,1}.cv; zOld=z; 
    else z=zOld*0.2;
    %iterations = iterations + 1;
    Nit = Nit - 1;
    data{1,1}.cv = cvOld + z*Grads;
    zOld=z; 
    continue 
    end

    data{1,1}.cv = data{1,1}.cv + z* Grads;
  
    NNpa=(Nv-Pe)*100/Nv;
    posErr=posErr+1;
    err(posErr,1)=error;
    
    if(NNpa_old <= NNpa), NNpa_old = NNpa; cvBest = data{1,1}.cv; end;
    
if(PRINTtoFILE)         
    fprintf( fid,'%d    %f \t   %f \t\t %f \t\t %0.5f \t\t %0.5f \n',Nit, error,NNpa, z,XN, XD);
else
    fprintf('%d    %f \t   %f \t\t %f \t\t %0.5f \t\t %0.5f \n',Nit, error,NNpa, z,XN, XD);
    title('error during center vector optimization');
   
end
end
figure(2);
plot(err);
fprintf('--------------------------------------------------------------------------------------------------\n\n');
if(PRINTtoFILE), fclose(fid); end;
data{1,1}.cv = cvBest;
end

