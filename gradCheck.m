function [ grad ] = gradCheck(J,W)
%% Created by Jugal Sheth 06/01/2016

grad=zeros(size(W));
EPSILON = 0.0001;
for i = 1:numel(W);
    e=zeros(size(W));
    e(i)=1;
    theta1=W+(e*EPSILON); theta2=W-(e*EPSILON);
    grad(i)=(J(theta1)-J(theta2))/(2*EPSILON);
end

end

