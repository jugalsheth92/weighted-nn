function [ grad ] = CV_gradientCheck( data,K,W,Nc)
%Created by Jugal Sheth 06/01/2016

cv=data{1}.cv; tCv=data{1}.cvLabels; 
train=data{2}.train; tTrain=data{2}.trainLabels;
Nv=size(tTrain,1); NvTrain=size(tCv,1); N=size(cv,2);

grad=zeros(1,N);
EPSILON = 0.0001;
for i = 1:NvTrain
    for n = 1:N
        e=zeros(size(cv));
        e(i,n)=1;
        ee = (e*EPSILON);
        theta1=cv+ee; theta2=cv-ee;
        J1=FuncSoftmax(train,tTrain,theta1,tCv,W,K,Nc);
        J2=FuncSoftmax(train,tTrain,theta2,tCv,W,K,Nc);
        grad(i,n)=(J1-J2)./(2*EPSILON);
    end
end



end

