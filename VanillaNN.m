%% This scripts classifies Gongsort using naive nearest neighbor classifier

clear;
trainingFile = 'Gongtrn.dat';
testingFile = 'Gongtst.tst';
N = 16; M = 1;
[train,tTrain,NvTrain] = read_approx_file(trainingFile, N, M);
[test,tTest,NvTest] = read_approx_file(testingFile, N, M);
NNpe = 0; KNNpe = 0; K = 3;
Kclass = zeros(K,1);
%% Classifing testing feature vector (Naive) & Knn with K = 5;
tic;
for i = 1:NvTest
    temp = repmat(test(i,:),[NvTrain,1]);
    euclidean = sum((train - temp).^2,2);
    [B,Idx] = sort(euclidean);
    Kclass(1:end,1) = tTrain(Idx(1:K,1),1);
    [Knn,F] = mode(Kclass);
    if (tTest(i)~=tTrain(Idx(1))) 
    NNpe=NNpe+1;
    end;
    KNNpe = KNNpe + (tTest(i)~=Knn);  
end
toc;
NNpa = (NvTest-NNpe)*100/NvTest; %nearest neighbor percentage accuracy
KNNpa = (NvTest-KNNpe)*100/NvTest; %K nearest neighbor percentage accuracy
fprintf('-- The percentage accuracy(naive) is %f \n', NNpa);
fprintf('-- The percentage accuracy(KNN) is %f \n', KNNpa);


