function [error, NNpa, W] = NNSoftmaxTrain(data,K,W,Nc)

% This function trains weights for NN
%fid=fopen('output.txt','W');
fprintf('WEIGHT OPTIMIZATION \n\n\n');
fprintf('Iter Train Err\t   Train Paccuracy \n\n');

%Initializing variables
EOld = 10000; ELeast=10000;
Nv = size(data{2,1}.train,1);
iterations=25; eOld=0; Nit = 0; posErr=0;
GRADCHECK = false; %% True, to check gradient calculation
% Iterations
while ( iterations ~= Nit)
Nit=Nit+1;    

[error,e,Pe] = NNSoftmaxCost_MOLF(data,K,W,Nc);
if (GRADCHECK && Nit==1)
    Grad=-gradCheck(@(x) NNSoftmaxCost_MOLF(data,K,x,Nc), W);
    diff = norm(G-Grad)/norm(G+Grad);
    display(diff);
end

if(error < EOld), EOld=error; WOld = W; eOld=e; 

else e=eOld*0.2;
W = WOld+(e);
eOld=e;
continue
end
posErr=posErr+1;
W = W+e;    
  
NNpa=(Nv-Pe)*100/Nv;
err(posErr,1)=error;
    
     
fprintf('%d    %f \t   %f \t\t\n',Nit, error,NNpa);
end
fprintf('--------------------------------------------------------------------------------------------------\n\n');
%fclose(fid);
figure(1);
plot(err);
title('error during weight optimization');
pause(0.1);

end

