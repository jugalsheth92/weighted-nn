function [ error, G, Pe, z,d1,d2] = NNSigmoidCost( data,K,W,Nc)
%% This function computes the cost and gradients
%% Created by Jugal Sheth 06/01/2016

least=zeros(Nc,1);
leastIndex=zeros(Nc,1);
train=data{1}.train; tTrain=data{1}.trainLabels; 
val=data{2}.val; tVal=data{2}.valLabels;
Nv=size(tVal,1); NvTrain=size(tTrain,1); N=size(train,2);
G=zeros(1,N);
d1=0; d2=0;
error=0;
Pe=0;
result(Nv).X=zeros(Nc,N); result(Nv).Y=zeros(Nc,Nv); 
result(Nv).T=zeros(Nc,Nv); %result(Nv).num=zeros(Nc,Nv); 
result(Nv).den=zeros(1,Nv); result(Nv).least=zeros(Nc,Nv);

repW=repmat(W.^2,[NvTrain,1]);
for p=1:Nv
    
    temp = repmat(val(p,:),[NvTrain,1]);
    distance =(sum((repW.*((train - temp).^2)),2))+0.0001;
    last=0;addn=0;
    for k = 1:Nc
        first=1+last;
        last=first+K(k,1)-1;
        [least(k,1),leastIndex(k,1)] =min(distance(first:last,:));
        leastIndex(k,1)=leastIndex(k,1)+addn;
        addn=addn+K(k,1);
    end

   
    Y=2./(1+exp(least));
    [~, class]=max(Y);
    T = zeros(Nc,1);
    T(tVal(p,1),1) = 1;     
    error = error+sum(1/Nv*((T-Y).^2)); 
    Pe = Pe+(class~=tVal(p,1));
    X=train(leastIndex,:); 
   
    %% Gradient Calculation
    
    den = - 2*exp(least)./((1+exp(least)).^2);
    
    den(isnan(den),1)=0;
    
    for m=1:N
          g = sum((2/Nv) .* (T - Y) .* 2.*W(1,m).*(X(:,m)-temp(1:Nc,m)).^ 2 .* den(:,1));
          G(1,m) = G(1,m) + g;
    end
      
    result(p).X=X; 
    result(p).Y=Y; 
%     result(p).num=num; 
    result(p).den=den;
%     result(p).least=least; 
     result(p).T=T;
end
    %OLF Calculation
for p=1:Nv
    temp=repmat(val(p,:),[Nc,1]);
    gradNum= sum(2 *  repmat(G,[Nc,1]) .* repmat(W,[Nc,1]) .*((result(p).X)-temp).^2,2);

    OD=(result(p).T)-(result(p).Y);

    dy=gradNum.*result(p).den;
    d2=d2+sum(dy.^2);
    d1=d1+sum(dy.* OD);

 end

z= d1/d2;
end


