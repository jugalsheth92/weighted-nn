function [ mk,label,Nv_k ] = kmeansplus(input,K)

%Author- Vineet Gundecha,6/9/2016

%function [ mk,label,Nv_k ] = kmeansplus(input,K)
%K-means clustering using k-means++ initialization
%Input arguments:
%input - datafile to cluster
%K - number of clusters
%Output arguments:
%mk - center vector for each cluster
%label - cluster label for each pattern
%Nv_k - number of patterns in each cluster

N = size(input,2);
Nv = size(input,1);
%Normalize the input
% input = (input - repmat(mean(input),Nv,1))./repmat(std(input),Nv,1);

c = zeros(1,Nv);
d = zeros(1,Nv);
Nv_k = zeros(1,K);
Ek = zeros(1,K);
mk = zeros(1,N);

%Initialization
%%
mk(1,:) = input(randi([1 Nv],1),:);
u = 1;
method = 3;
for k = 2:K
    
    dist = sum(((repelem(input,u,1) - repmat(mk,Nv,1)).^2),2);
    dist = reshape(dist,u,[]);
    d = min(dist,[],1);
    
    c = cumsum(d);
    
    f = d/c(Nv);
    c = c/c(Nv);
    
    %Method 1
    if method==1
        [maximum, indices] = max(f);
        u = u + 1;
        mk(u,:) = input(indices,:);
        %     disp(indices)
        
    elseif method==2
        %Method 2
        z = repmat(f',1,N);
        u = u+1;
        mk(u,:) = sum(z.*input);
        
    else
        %Method 3
        [row, col] = find(c>rand,1);
        u = u+1;
        mk(u,:) = input(col,:);
        
    end %end if
end

label = zeros(1,Nv);
old_label = ones(1,Nv);
it = 0;

%%


while(any(label~=old_label))
    it = it+1;
    old_label = label;
    Ek = zeros(1,K);
    rep = repmat(1:Nv,K,1);
    rep = rep(:);
    dst_temp = sum(((input(rep,:) - repmat(mk,Nv,1)).^2),2);
    dst = reshape(dst_temp,K,Nv);
    [min_dst label] = min(dst);
    xx = full(sparse(label,1:Nv,1));
    Nv_k = sum(xx,2);
    yy = bsxfun(@rdivide,xx,Nv_k);
    mk = yy*input;
    Ek = xx*min_dst';
    E(it) = sum(Ek)/Nv;
    
end
end

